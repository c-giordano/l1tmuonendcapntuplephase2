
import FWCore.ParameterSet.Config as cms
from Configuration.Eras.Modifier_phase2_GE0_cff import phase2_GE0

# EMTF Tools Ntuple Maker Configuration
emtfToolsNtupleMaker = cms.EDAnalyzer(
    "EMTFToolsNtupleMaker",

    # Verbosity level
    verbosity = cms.untracked.int32(3),

    # Enables
    EventInfoEnabled = cms.bool(True),
    EMTFP2HitsEnabled = cms.bool(True),
    EMTFP2TracksEnabled = cms.bool(True),
    EMTFP2InputsEnabled = cms.bool(True),
    EMTFP2SimInfoEnabled = cms.bool(True),
    L1TrackTriggerTracksEnabled = cms.bool(False),
    TrackingParticlesEnabled = cms.bool(True),
    CSCSimHitEnabled = cms.bool(True),
    RPCSimHitEnabled = cms.bool(True),
    GEMSimHitEnabled = cms.bool(True),
    ME0SimHitEnabled = cms.bool(True),
    GenParticlesEnabled = cms.bool(True),

    # Alt Enables
    CrossingFrameEnabled = cms.bool(False),

    # Tags
    GenPartTag = cms.InputTag('genParticles'),
    simTrackTag = cms.InputTag('g4SimHits'),
    TrkPartTag = cms.InputTag('mix', 'MergedTrackTruth'),
    PileupInfoTag = cms.InputTag('addPileupInfo'),
    EMTFP2HitTag = cms.InputTag('simEmtfDigisPhase2'),
    EMTFP2TrackTag = cms.InputTag('simEmtfDigisPhase2'),
    EMTFP2InputTag = cms.InputTag('simEmtfDigisPhase2'),
    TTTFTrackTag = cms.InputTag('TTTracksFromTrackletEmulation', 'Level1TTTracks'),
    TTTFTrackAssocTag = cms.InputTag('TTTrackAssociatorFromPixelDigis', 'Level1TTTracks'),

    CSCSimHitsTag = cms.InputTag('g4SimHits', 'MuonCSCHits'),
    CSCSimHitsXFTag = cms.InputTag('mix', 'g4SimHitsMuonCSCHits'),
    RPCSimHitsTag = cms.InputTag('g4SimHits', 'MuonRPCHits'),
    RPCSimHitsXFTag = cms.InputTag('mix', 'g4SimHitsMuonRPCHits'),
    GEMSimHitsTag = cms.InputTag('g4SimHits', 'MuonGEMHits'),
    GEMSimHitsXFTag = cms.InputTag('mix', 'g4SimHitsMuonGEMHits'),
    ME0SimHitsTag = cms.InputTag('g4SimHits', 'MuonME0Hits'),
    ME0SimHitsXFTag = cms.InputTag('mix', 'g4SimHitsMuonME0Hits'),

    CSCStripSimLinksTag = cms.InputTag('simMuonCSCDigis', 'MuonCSCStripDigiSimLinks'),
    CSCWireSimLinksTag  = cms.InputTag('simMuonCSCDigis', 'MuonCSCWireDigiSimLinks'),
    RPCDigiSimLinksTag  = cms.InputTag('simMuonRPCDigis', 'RPCDigiSimLink'),
    GEMDigiSimLinksTag  = cms.InputTag('simMuonGEMDigis', 'GEM'),
    ME0DigiSimLinksTag  = cms.InputTag('simMuonME0Digis', 'ME0'),

    # muon track extrapolation to 1st station
    MuonPropagatorSt1 = cms.PSet(
        useTrack = cms.string("tracker"),  # 'none' to use Candidate P4; or 'tracker', 'muon', 'global'
        useState = cms.string("atVertex"), # 'innermost' and 'outermost' require the TrackExtra
        useSimpleGeometry = cms.bool(True),
        useStation2 = cms.bool(False),
        fallbackToME1 = cms.bool(False),
        cosmicPropagationHypothesis = cms.bool(False),
        useMB2InOverlap = cms.bool(False),
        propagatorAlong = cms.ESInputTag("", "SteppingHelixPropagatorAlong"),
        propagatorAny = cms.ESInputTag("", "SteppingHelixPropagatorAny"),
        propagatorOpposite = cms.ESInputTag("", "SteppingHelixPropagatorOpposite")
    ),

    # muon track extrapolation to 2nd station
    MuonPropagatorSt2 = cms.PSet(
        useTrack = cms.string("tracker"),  # 'none' to use Candidate P4; or 'tracker', 'muon', 'global'
        useState = cms.string("atVertex"), # 'innermost' and 'outermost' require the TrackExtra
        useSimpleGeometry = cms.bool(True),
        useStation2 = cms.bool(True),
        fallbackToME1 = cms.bool(False),
        cosmicPropagationHypothesis = cms.bool(False),
        useMB2InOverlap = cms.bool(False),
        propagatorAlong = cms.ESInputTag("", "SteppingHelixPropagatorAlong"),
        propagatorAny = cms.ESInputTag("", "SteppingHelixPropagatorAny"),
        propagatorOpposite = cms.ESInputTag("", "SteppingHelixPropagatorOpposite")
    )
)

phase2_GE0.toModify(emtfToolsNtupleMaker, ME0SimHitEnabled=False)

