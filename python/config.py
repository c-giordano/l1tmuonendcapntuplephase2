import FWCore.ParameterSet.Config as cms

def customise_ntuple(process):
    # EMTF Tools
    process.load('EMTFTools.NtupleMaker.ntuple_maker_cfi')

    process.EMTFToolsNtupleMakerSequence = cms.Sequence(
        process.emtfToolsNtupleMaker
    )

    # Path
    process.EMTFToolsNtupleMaker_step = cms.Path(process.EMTFToolsNtupleMakerSequence)

    process.schedule.extend([process.EMTFToolsNtupleMaker_step])

    # Remove cms.EndPath instances from schedule
    paths_in_schedule = [path for path in process.schedule if not isinstance(path, cms.EndPath)]
    process.schedule = cms.Schedule(*paths_in_schedule)
    return process

def remove_L1TrackTrigger_step(process):
    # Remove L1TrackTrigger_step from schedule
    if process.L1TrackTrigger_step in process.schedule:
        process.schedule.remove(process.L1TrackTrigger_step)

    return process

def remove_L1simulation_step(process):
    # Remove L1simulation_step from schedule
    if process.L1simulation_step in process.schedule:
        process.schedule.remove(process.L1simulation_step)

    return process
