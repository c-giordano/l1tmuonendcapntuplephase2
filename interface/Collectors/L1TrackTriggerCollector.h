#ifndef EMTFTools_NtupleMaker_L1TrackTriggerCollector_h
#define EMTFTools_NtupleMaker_L1TrackTriggerCollector_h

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/EventSetup.h"
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/ConsumesCollector.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"

#include "DataFormats/L1TrackTrigger/interface/TTTrack.h"
#include "DataFormats/L1TrackTrigger/interface/TTTypes.h"
#include "EMTFTools/NtupleMaker/interface/NtupleMakerContext.h"
#include "EMTFTools/NtupleMaker/interface/Collectors/DataCollectors.h"
#include "SimTracker/TrackTriggerAssociation/interface/TTTrackAssociationMap.h"

namespace emtf::tools {

    typedef TTTrack<Ref_Phase2TrackerDigi_> TTTFTrack;
    typedef std::vector<TTTFTrack> TTTFTrackCollection;
    typedef TTTrackAssociationMap<Ref_Phase2TrackerDigi_> TTTFTrackAssociator;

    class L1TrackTriggerCollector: public DataCollector {
        public:
            L1TrackTriggerCollector(
                    const NtupleMakerContext&,
                    const edm::ParameterSet&,
                    edm::ConsumesCollector&&
            );

            ~L1TrackTriggerCollector();

            void registerFields(DynamicTree&) const final;

            void collect(
                    const edm::Event&, const edm::EventSetup&, 
                    DynamicTree&
            ) const final;

        private:
            const NtupleMakerContext& context_;

            edm::EDGetTokenT<TTTFTrackCollection> track_token_;
            edm::EDGetTokenT<TTTFTrackAssociator> track_assoc_token_;
    };

}  // namespace emtf::tools

#endif  // EMTFTools_NtupleMaker_L1TrackTriggerCollector_h
