#ifndef EMTFTools_NtupleMaker_EMTFP2TracksCollector_h
#define EMTFTools_NtupleMaker_EMTFP2TracksCollector_h

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/EventSetup.h"
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/ConsumesCollector.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"

#include "EMTFTools/NtupleMaker/interface/NtupleMakerContext.h"
#include "EMTFTools/NtupleMaker/interface/Collectors/DataCollectors.h"
#include "L1Trigger/L1TMuonEndCapPhase2/interface/EMTFTypes.h"

namespace emtf::tools {

    class EMTFP2TracksCollector: public DataCollector {
        public:
            EMTFP2TracksCollector(
                    const NtupleMakerContext&,
                    const edm::ParameterSet&,
                    edm::ConsumesCollector&&
            );

            ~EMTFP2TracksCollector();

            void registerFields(DynamicTree&) const final;

            void collect(
                    const edm::Event&, 
                    const edm::EventSetup&, 
                    DynamicTree&
            ) const final;

        private:
            const NtupleMakerContext& context_;

            edm::EDGetTokenT<emtf::phase2::EMTFTrackCollection> token_;
    };

}  // namespace emtf::tools

#endif  // EMTFTools_NtupleMaker_EMTFP2TracksCollector_h
