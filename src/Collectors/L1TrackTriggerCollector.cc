#include "EMTFTools/NtupleMaker/interface/Fields.h"
#include "EMTFTools/NtupleMaker/interface/Utils/EMTFUtils.h"

#include "EMTFTools/NtupleMaker/interface/Collectors/L1TrackTriggerCollector.h"

using namespace emtf::tools;

L1TrackTriggerCollector::L1TrackTriggerCollector(
        const NtupleMakerContext& context, 
        const edm::ParameterSet&  pset,
        edm::ConsumesCollector&&  consumes_collector
): 
    context_(context)
{
    track_token_ = consumes_collector.consumes<TTTFTrackCollection>(
            pset.getParameter<edm::InputTag>("TTTFTrackTag")  
    );
    
    track_assoc_token_ = consumes_collector.consumes<TTTFTrackAssociator>(
            pset.getParameter<edm::InputTag>("TTTFTrackAssocTag")  
    );
}

L1TrackTriggerCollector::~L1TrackTriggerCollector() {
    // Do Nothing
}

void L1TrackTriggerCollector::registerFields(DynamicTree& tree) const {
    tree.decfield<tag_l1t_trk_pt>();
    tree.decfield<tag_l1t_trk_phi>();
    tree.decfield<tag_l1t_trk_theta>();
    tree.decfield<tag_l1t_trk_eta>();
    tree.decfield<tag_l1t_trk_vx>();
    tree.decfield<tag_l1t_trk_vy>();
    tree.decfield<tag_l1t_trk_vz>();
    tree.decfield<tag_l1t_trk_q>();
    tree.decfield<tag_l1t_trk_rinv>();
    tree.decfield<tag_l1t_trk_chi2>();
    tree.decfield<tag_l1t_trk_ndof>();
    tree.decfield<tag_l1t_trk_phi_sector>();
    tree.decfield<tag_l1t_trk_eta_sector>();
    tree.decfield<tag_l1t_trk_hit_pattern>();
    tree.decfield<tag_l1t_trk_sim_pt>();
    tree.decfield<tag_l1t_trk_sim_phi>();
    tree.decfield<tag_l1t_trk_sim_eta>();
    tree.decfield<tag_l1t_trk_sim_tp>();
    tree.decfield<tag_l1t_trk_pdgid>();
    tree.decfield<tag_l1t_trk_genuine>();
    tree.decfield<tag_vsize_l1t_trk>();
}

void L1TrackTriggerCollector::collect(
        const edm::Event& event,
        const edm::EventSetup& event_setup, 
        DynamicTree& tree
) const {
    // Get L1T Track Trigger Tracks
    auto tracks_handle = event.getHandle(track_token_);

    auto trk_collection = reinterpret_cast<const TTTFTrackCollection*>(
            tracks_handle.product()
    );

    // Get L1T Track Trigger Track Associator
    auto track_assoc_handle = event.getHandle(track_assoc_token_);

    auto trk_assoc = reinterpret_cast<const TTTFTrackAssociator*>(
            track_assoc_handle.product()
    );

    // Short-Circuit: If not available leave
    if (trk_collection == nullptr or trk_assoc == nullptr) {
        return;
    }

    // Collect Traking Particles
    int i_trk = -1;

    for (const auto& trk : (*trk_collection)) {
        // Get Track Pointer
        edm::Ptr<TTTFTrack> trk_ptr(tracks_handle, ++i_trk);

        // Get Track Tracking Particle
        edm::Ptr<TrackingParticle> tp_ptr = trk_assoc->findTrackingParticlePtr(trk_ptr);

        // Get Tracking Particle Info
        float sim_pt = 0;
        float sim_phi = 0;
        float sim_eta = 0;
        int sim_tp = -1;
        int sim_pdgid = 0;
        int sim_genuine = 0;

        if (tp_ptr.isNonnull()) {
            sim_pt = tp_ptr->pt();
            sim_phi = tp_ptr->phi();
            sim_eta = tp_ptr->eta();
            sim_tp = tp_ptr.key();
            sim_pdgid = tp_ptr->pdgId();
        }

        // Get Association Info
        if (trk_assoc->isGenuine(trk_ptr)) {
            sim_genuine = 3;
        } else if (trk_assoc->isLooselyGenuine(trk_ptr)) {
            sim_genuine = 2;
        } else if (trk_assoc->isCombinatoric(trk_ptr)) {
            sim_genuine = 1;
        } else if (trk_assoc->isUnknown(trk_ptr)) {
            sim_genuine = 0;
        }

        // Get Track Info
        const GlobalVector& momentum = trk.momentum();
        const GlobalPoint& poca = trk.POCA();
        const double rinv = trk.rInv();

        // Append Info
        tree.get<tag_l1t_trk_pt>().push_back(momentum.perp());
        tree.get<tag_l1t_trk_phi>().push_back(momentum.phi());
        tree.get<tag_l1t_trk_theta>().push_back(momentum.theta());
        tree.get<tag_l1t_trk_eta>().push_back(momentum.eta());
        tree.get<tag_l1t_trk_vx>().push_back(poca.x());
        tree.get<tag_l1t_trk_vy>().push_back(poca.y());
        tree.get<tag_l1t_trk_vz>().push_back(poca.z());
        tree.get<tag_l1t_trk_q>().push_back(rinv >= 0 ? 1 : -1);
        tree.get<tag_l1t_trk_rinv>().push_back(rinv);
        tree.get<tag_l1t_trk_chi2>().push_back(trk.chi2());
        tree.get<tag_l1t_trk_ndof>().push_back(trk.getStubRefs().size() * 2 - trk.nFitPars());
        tree.get<tag_l1t_trk_phi_sector>().push_back(trk.phiSector());
        tree.get<tag_l1t_trk_eta_sector>().push_back(trk.etaSector());
        tree.get<tag_l1t_trk_hit_pattern>().push_back(trk.hitPattern());
        tree.get<tag_l1t_trk_sim_pt>().push_back(sim_pt);
        tree.get<tag_l1t_trk_sim_phi>().push_back(sim_phi);
        tree.get<tag_l1t_trk_sim_eta>().push_back(sim_eta);
        tree.get<tag_l1t_trk_sim_tp>().push_back(sim_tp);
        tree.get<tag_l1t_trk_pdgid>().push_back(sim_pdgid);
        tree.get<tag_l1t_trk_genuine>().push_back(sim_genuine);
    }

    tree.set<tag_vsize_l1t_trk>(trk_collection->size());
}

